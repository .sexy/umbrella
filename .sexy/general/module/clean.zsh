#!/usr/bin/env zsh

typeset -ga __SEXY_CLEANUP_V
typeset -ga __SEXY_CLEANUP_F

function sexy-cleanup-queue-f() { __SEXY_CLEANUP_F+=("$@") }
function sexy-cleanup-queue-v() { __SEXY_CLEANUP_V+=("$@") }

function sexy-cleanup() {
    setopt localoptions

    unset -f $__SEXY_CLEANUP_F[@]
    unset    $__SEXY_CLEANUP_V[@]

    unset -f sexy-cleanup-queue-f
    unset -f sexy-cleanup-queue-v
    unset -f sexy-cleanup

    unset    __SEXY_CLEANUP_F
    unset    __SEXY_CLEANUP_V
}
